package ru.tsc.ichaplygina.taskmanager.repository;

import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        write(new Project("TEST1"));
        write(new Project("TEST2"));
        write(new Project("TEST3"));
        write(new Project("TEST4"));
    }

    public void create() {
        write(new Project(("New Project" + System.currentTimeMillis())));
    }

    public void write(final Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

}
