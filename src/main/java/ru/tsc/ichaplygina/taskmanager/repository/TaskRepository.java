package ru.tsc.ichaplygina.taskmanager.repository;

import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {
    
    private static final TaskRepository INSTANCE = new TaskRepository();
    
    public static TaskRepository getInstance() {
        return INSTANCE;
    } 
    
    private final Map<String, Task> tasks = new LinkedHashMap<>();
    
    {
        write(new Task("TEST1"));
        write(new Task("TEST2"));
        write(new Task("TEST3"));
        write(new Task("TEST4"));
    }
    
    public void create() {
        write(new Task(("New Task" + System.currentTimeMillis())));
    }
    
    public void write(final Task task) {
        tasks.put(task.getId(), task);
    }
    
    public Collection<Task> findAll() {
        return tasks.values();
    }
    
    public Task findById(final String id) {
        return tasks.get(id);
    }
    
    public void removeById(final String id) {
        tasks.remove(id);
    }
        
}
