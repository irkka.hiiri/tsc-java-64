<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Task List</h1>
<table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <th width="200" nowrap="nowrap">id</th>
        <th width="200" nowrap="nowrap" align="left">name</th>
        <th width="500">description</th>
        <th width="100">created</th>
        <th width="100">started</th>
        <th width="100">finished</th>
        <th width="100">status</th>
        <th width="100">project id</th>
        <th width="100" nowrap="nowrap" align="center">Edit</th>
        <th width="100" nowrap="nowrap" align="center">Remove</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}" />
            </td>
            <td>
                <c:out value="${task.name}" />
            </td>
            <td>
                <c:out value="${task.description}" />
            </td>
            <td>
                <fmt:formatDate value="${task.created}" pattern="dd.MM.yyyy" />
            </td>
            <td>
                <fmt:formatDate value="${task.dateStart}" pattern="dd.MM.yyyy" />
            </td>
            <td>
                <fmt:formatDate value="${task.dateFinish}" pattern="dd.MM.yyyy" />
            </td>
            <td>
                <c:out value="${task.status}" />
            </td>
            <td>
                <c:out value="${task.projectId}" />
            </td>
            <td align="center">
                <a href="/task/edit/${task.id}">Edit</a>
            </td>
            <td align="center">
                <a href="/task/delete/${task.id}">Remove</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/task/create" style="margin-top: 20px">
    <button>Create</button>
</form>

<jsp:include page="../include/_footer.jsp"/>
